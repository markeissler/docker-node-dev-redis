#!/usr/bin/env bash
#
# docker-entrypoint.sh for app
#

configPostgres() {
    if [[ ! -f "${SOURCE_DIR}/config/database.yml" ]]; then
        printf "%s\n-> " "-> Configuring Postgres..."

cat <<- 'EOF' > "${SOURCE_DIR}/config/database.yml"
default: &default
  adapter: postgresql
  encoding: unicode
  pool: 5
  username: <%= ENV['POSTGRES_USER'] %>
  password: <%= ENV['POSTGRES_PASSWORD'] %>
  host: <%= ENV['POSTGRES_HOST'] %>
  port: <%= ENV['POSTGRES_PORT'] %>

development:
  <<: *default
  database: <%= ENV['APPNAME'] %>_development

test:
  <<: *default
  database: <%= ENV['APPNAME'] %>_test
EOF
        # set perms
        chmod 0644 "${SOURCE_DIR}/config/database.yml"
        chown "${BUILD_USER}":"${BUILD_USER}" "${SOURCE_DIR}/config/database.yml"

        printf "%s\n" "-> Postgres configured"
    fi
}

initPostgres() {
    if [[ -n "${APPHOST// }" ]]; then
        printf "%s\n" "-> Initializing Postgres database..."

        # check that db is reachable
        while ! nc -z "${POSTGRES_HOST}" "${POSTGRES_PORT}"; do
            printf "Waiting for ${POSTGRES_HOST} to be reachable\n"
            sleep 2
        done

        #
        # Create app db
        #
        rake db:create
        if [[ ! -f "${SOURCE_DIR}/db/schema.db" ]]; then
            rake db:migrate
        else
            rake db:schema:load
        fi
        rake db:seed

        ##
        ## >RAILS_ENV=test rake warehouse:db:create
        ##
        printf "%s\n\n" "-> Postgres database initialized"
    fi
}

configVolumes() {
    #
    # Setup volume permissions
    #
    printf "%s\n-> " "-> Setting volume permissions..."
    sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/node_modules"
    printf "%s\n" "-> Volume permissions set"
}

updateEnvironment() {
    #
    # Update environment
    #
    if [[ -f "${SOURCE_DIR}/.envrc" ]]; then
        printf "%s\n-> " "-> Updating environment..."
        pushd "${PWD}"
        cd "${SOURCE_DIR}"
        direnv allow
        popd
        printf "%s\n-> " "-> Environment updated"
    fi
}

updateModules() {
    #
    # Update modules
    #
    printf "%s\n-> " "-> Updating modules..."
    npm install
    printf "%s\n" "-> Modules updated"
}

#
# Check if app is mounted in source before continuing
#
printf "\n%s\n" "Checking configuration..."
if ! [[ -f "${SOURCE_DIR}/package.json" || -f "${SOURCE_DIR}/Gopkg.toml" ]]; then
    printf "\n%s\n" "-> App not found!"
cat <<- EOF

You must start this container with the app mounted in the working directory:

    ${SOURCE_DIR}

Refer to the dockerfiles/DOCKER.md file documentation for help.

EOF
    exit 1
fi

#
# Configure
#
printf "%s\n" "-> App found. Configuring..."

# update volume permissions
configVolumes

# always run in app source directory!
cd "${SOURCE_DIR}" 2>&1 > /dev/null

# check for previously installed modules
if [[ ! -f "${BUILD_HOME}/.locks/.bootstrap.lock" ]]; then
    touch "${BUILD_HOME}/.locks/.bootstrap.lock"
    updateEnvironment
    configVolumes
    updateModules
else
    updateEnvironment
    updateModules
fi

# make sure node_modules is owned by the app user
sudo chown "${BUILD_USER}:${BUILD_USER}" "${SOURCE_DIR}/node_modules"

printf "%s\n\n" "Configuration finished"

#
# Start Xvfb. Redirect output to null because it complains when starting from
# non-priveleged mode.
#
Xvfb :99 -screen 0 1280x1024x24 > /dev/null 2>&1 &

#
# Run command (usually starts the target app)
#

# run any other command passed on cli
exec "$@"
