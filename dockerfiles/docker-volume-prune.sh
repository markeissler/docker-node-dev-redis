#!/usr/bin/env bash

volumelist=(
	${APPNAME}_persist-app-modules
	${APPNAME}_persist-postgres
	${APPNAME}_persist-redis
)

for v in "${volumelist[@]}"; do
	docker volume rm "${v}"
done
